set -eux
docker-compose up --detach mongoserver
docker-compose up --exit-code-from mongoclient mongoclient
docker-compose down